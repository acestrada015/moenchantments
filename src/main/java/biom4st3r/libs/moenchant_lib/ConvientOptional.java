package biom4st3r.libs.moenchant_lib;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

@AvailableSince("0.1.1")
public final class ConvientOptional<T> {

    @SuppressWarnings({"rawtypes"})
    private static final ConvientOptional EMPTY = new ConvientOptional<>(null);
    final Object o;
    private ConvientOptional(Object o) {
        this.o = o;
    }

    @SuppressWarnings({"unchecked"})
    public <E> E get() {
        if (this == EMPTY) throw new IllegalStateException("ConvientOptional#get cannot be called on EMPTY");
        return (E) this.o;
    }
    
    @SuppressWarnings({"unchecked"})
    public <E> E getOrNull() {
        return (E) this.o;
    }

    public boolean isEmpty() {
        return this == EMPTY;
    }

    @SuppressWarnings({"unchecked"})
    public static final <T> ConvientOptional<T> empty() {
        return EMPTY;
    }

    @SuppressWarnings({"unchecked"})
    public static <T> ConvientOptional<T> of(T t) {
        if (t == null) {
            return (ConvientOptional<T>) EMPTY;
        } else {
            return new ConvientOptional<>(t);
        }
    }
}
