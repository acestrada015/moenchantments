package biom4st3r.libs.moenchant_lib.events;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.ScheduledForRemoval;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;

@AvailableSince("0.1.0")
public interface OnCrossBowArrowCreationEvent {
    @Deprecated(forRemoval = true)
    @ScheduledForRemoval(inVersion = "0.4.5")
    Event<OnCrossBowArrowCreationEvent> EVENT = EventFactory.createArrayBacked(OnCrossBowArrowCreationEvent.class,
        listeners  ->  (arrow, crossbow, arrowItem, le)  ->  {
            for (OnCrossBowArrowCreationEvent event : listeners) {
                event.onCreation(arrow, crossbow, arrowItem, le);
            }
    });
    void onCreation(PersistentProjectileEntity arrow, ItemStack crossBow, ItemStack arrowItem, LivingEntity shooter);
}