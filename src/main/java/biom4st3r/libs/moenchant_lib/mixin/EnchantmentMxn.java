package biom4st3r.libs.moenchant_lib.mixin;

import net.minecraft.enchantment.Enchantment;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({Enchantment.class})
public abstract class EnchantmentMxn implements ExtendedEnchantment {
    @Override
    public boolean isExtended() {
        return false;
    }
}
