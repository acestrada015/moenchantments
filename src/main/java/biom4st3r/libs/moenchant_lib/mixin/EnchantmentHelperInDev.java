package biom4st3r.libs.moenchant_lib.mixin;

import java.util.Iterator;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.registry.Registry;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(EnchantmentHelper.class)
public class EnchantmentHelperInDev {
    @Redirect(method = "getPossibleEntries", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/registry/Registry;iterator()Ljava/util/Iterator;", ordinal = 0, remap = false))
    private static Iterator<Enchantment> preventMoEnchantments(Registry<Enchantment> reg) {
        return reg.stream().filter(e  ->  !ExtendedEnchantment.cast(e).isExtended()).iterator();
    }
}
