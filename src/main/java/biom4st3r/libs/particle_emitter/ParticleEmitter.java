package biom4st3r.libs.particle_emitter;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.Internal;

import com.google.common.collect.Maps;

import biom4st3r.libs.particle_emitter.interfaces.ParticleEmittingWorld;
import biom4st3r.net.objecthunter.exp4j.Expression;
import biom4st3r.net.objecthunter.exp4j.ExpressionBuilder;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.ParticleS2CPacket;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleType;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Util;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;

public class ParticleEmitter {
    final ParticleEffect type;
    final Vec3d pos;
    final String stringVelocityX;
    final String stringVelocityY;
    final String stringVelocityZ;
    final Expression velocityX;
    final Expression velocityY;
    final Expression velocityZ;
    boolean dead;
    final World world;
    final Random random;
    public final long ID;
    int lifetime;

    public static interface ParticleContext {
        double test(int tick, Random rand, Vec3d pos);
    }

    /**
     * 
     * @param type
     * @param pos
     * @param vx are mathmatical expresssions supporting the varibles randomdouble, randomint, randomgaussian, tick, pos.x, pos.y, pos.z
     * @param vy
     * @param vz
     * @param world
     * @param lifeTime
     * @return
     */
    @AvailableSince("0.1.0")
    public static Optional<ParticleEmitter> of(ParticleEffect type, Vec3d pos, String vx, String vy,
        String vz, World world, int lifeTime) {
        if (world.isClient) return Optional.empty(); 
        if (lifeTime == 1) {
            float veloX = (float)fillExpression(lifeTime, world.random, pos, getBuilder(vx).build()).evaluate();
            float veloY = (float)fillExpression(lifeTime, world.random, pos, getBuilder(vy).build()).evaluate();
            float veloZ = (float)fillExpression(lifeTime, world.random, pos, getBuilder(vz).build()).evaluate();
            final ParticleS2CPacket packet = new ParticleS2CPacket(type, true, pos.x, pos.y, pos.z, veloX, veloY, veloZ, 1, 0);
            forPlayerInRange(world, pos, player -> {
                player.networkHandler.connection.send(packet);
            });
            return Optional.empty();
        }
        return Optional.of(new ParticleEmitter(type, pos, vx, vy, vz, world, lifeTime));
    }

    @AvailableSince("tbd")
    public static Optional<List<ParticleEmitter>> of(int count, ParticleEffect type, Vec3d pos, Function<Random, Vec3d> offset, String vx, String vy,
        String vz, World world, int lifeTime) {
        if (world.isClient) return Optional.empty();
        if (lifeTime == 1) {
            for (int i = 0; i < count; i++) {
                Vec3d o = offset.apply(world.random);
                float veloX = (float)fillExpression(lifeTime, world.random, pos, getBuilder(vx).build()).evaluate();
                float veloY = (float)fillExpression(lifeTime, world.random, pos, getBuilder(vy).build()).evaluate();
                float veloZ = (float)fillExpression(lifeTime, world.random, pos, getBuilder(vz).build()).evaluate();
    
                final ParticleS2CPacket packet = new ParticleS2CPacket(type, true, pos.getX()+o.x, pos.getY()+o.y, pos.getZ()+o.z, veloX, veloY, veloZ, 1, 0);
                forPlayerInRange(world, pos, player -> {
                    player.networkHandler.connection.send(packet);
                });
            }
            return Optional.empty();
        }
        // List<ParticleEmitter> list = Lists.newArrayList();
        // TODO


        return Optional.empty();
    }

    @AvailableSince("0.1.0")
    public void begin() {
        if (this.world.isClient) {
            throw new IllegalAccessError("Partical Emitters should never be started from the client. Honestly i'm not even sure how you got one of these on your client; I explicitly block that from happening...unless you were messing around in ClientWorld in which case, why? that seems so unnessisary?");
        }
        ((ParticleEmittingWorld)this.world).addEmitter(this);
    }

    /**
     * @param type
     * @param pos
     * @param velocityX
     * @param velocityY
     * @param velocityZ
     * @param dead
     * @param world
     */
    @Internal
    private ParticleEmitter(ParticleEffect type, Vec3d pos, String vx, String vy,
            String vz, World world, int lifeTime) {
        this.type = type;
        this.pos = pos;
        this.stringVelocityX = vx;
        this.stringVelocityY = vy;
        this.stringVelocityZ = vz;
        this.velocityX = getBuilder(vx).build();
        this.velocityY = getBuilder(vy).build();
        this.velocityZ = getBuilder(vz).build();
        this.dead = false;
        this.world = world;
        this.random = Random.create(world.random.nextLong());
        this.lifetime = lifeTime;
        this.ID = random.nextLong();
    }

    @Internal
    private ParticleEmitter(ParticleEffect type, Vec3d pos, String vx, String vy,
        String vz, World world, int lifeTime, long id) {
        this.type = type;
        this.pos = pos;
        this.stringVelocityX = vx;
        this.stringVelocityY = vy;
        this.stringVelocityZ = vz;
        this.velocityX = getBuilder(vx).build();
        this.velocityY = getBuilder(vy).build();
        this.velocityZ = getBuilder(vz).build();
        this.dead = false;
        this.world = world;
        this.random = Random.create(world.random.nextLong());
        this.lifetime = lifeTime;
        this.ID = id;
    }

    private static ExpressionBuilder getBuilder(String expression) {
        ExpressionBuilder builder = new ExpressionBuilder(expression);
        builder.variables(variables.keySet());
        return builder;
    }

    public static final String 
        RANDOM_DOUBLE = "randomdouble",
        RANDOM_GAUSS = "randomguassian",
        RANDOM_INT = "randomint",
        TICK = "tick",
        POSX = "pos.x",
        POSY = "pos.y",
        POSZ = "pos.z"
        ;
    
    private static final Map<String, ParticleContext> variables = Util.make(Maps.newHashMap(), map -> {
        map.put(RANDOM_DOUBLE, (ticks, rand, pos) -> rand.nextDouble());
        map.put(RANDOM_GAUSS, (ticks, rand, pos) -> rand.nextGaussian());
        map.put(RANDOM_INT, (ticks, rand, pos) -> rand.nextInt());
        map.put(TICK, (ticks, rand, pos) -> ticks);
        map.put(POSX, (ticks, rand, pos) -> pos.x);
        map.put(POSY, (ticks, rand, pos) -> pos.y);
        map.put(POSZ, (ticks, rand, pos) -> pos.z);
    });

    @AvailableSince("0.1.0")
    public static void registerVariable(String name, ParticleContext func) {
        variables.put(name, func);
    }

    private Expression fillExpression(Expression express) {
        for (String s : express.getVariableNames()) {
            ParticleContext i = variables.get(s);
            if (i != null) {
                express.setVariable(s, i.test(this.lifetime, this.random, this.pos));
            } else {
                throw new IllegalArgumentException(s + " is not a valid varibles for ParticleEmitter");
            }
        }
        return express;
    }

    private static Expression fillExpression(int lifetime, Random random, Vec3d pos, Expression expression) {
        for (String s : expression.getVariableNames()) {
            ParticleContext i = variables.get(s);
            if (i != null) {
                expression.setVariable(s, i.test(lifetime, random, pos));
            } else {
                throw new IllegalArgumentException(s + " is not a valid varibles for ParticleEmitter");
            }
        }
        return expression;
    }

    public boolean isDead() {
        return this.dead;
    }

    private IntSet leachers = new IntOpenHashSet();
    private IntSet peers = new IntOpenHashSet();

    @AvailableSince("0.1.0")
    public void kill() {
        this.dead = true;
        forPlayerInRange(player -> peers.contains(player.getId()), player -> {
            Packets.SERVER.killParticleEmiiter(player, this.ID);
        });
    }

    private static void forPlayerInRange(World world, Vec3d pos, Consumer<ServerPlayerEntity> consumer) {
        if (!world.isClient) {
            for (ServerPlayerEntity player : ((ServerWorld)world).getPlayers()) {
                if (pos.subtract(player.getPos()).length() < RANGE) {
                    consumer.accept(player);
                }
            }
        }
    }    
    private void forPlayerInRange(Consumer<ServerPlayerEntity> consumer) {
        if (!world.isClient) {
            for (ServerPlayerEntity player : ((ServerWorld)world).getPlayers()) {
                if (this.pos.subtract(player.getPos()).length() < RANGE) {
                    consumer.accept(player);
                }
            }
        }
    }
    private void forPlayerInRange(Predicate<ServerPlayerEntity> filter, Consumer<ServerPlayerEntity> consumer) {
        if (!world.isClient) {
            for (ServerPlayerEntity player : ((ServerWorld)world).getPlayers()) {
                if (!filter.test(player)) continue;
                if (this.pos.subtract(player.getPos()).length() < RANGE) {
                    consumer.accept(player);
                }
            }
        }
    }

    static final int RANGE = 120;

    @Internal
    @Environment(EnvType.CLIENT)
    @SuppressWarnings({"resource"})
    private void emitClient() {
        if (MinecraftClient.getInstance().player.getPos().subtract(pos).length() > RANGE) {
            this.kill();
            return;
        }
        switch(MinecraftClient.getInstance().options.getParticles().getValue()) {
            case ALL:
                world.addImportantParticle((ParticleEffect)type, true, pos.x, pos.y, pos.z, fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
                break;
            case DECREASED:
                if ((this.lifetime & 3) == 3) {
                    world.addImportantParticle((ParticleEffect)type, true, pos.x, pos.y, pos.z, fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
                }
                break;
            case MINIMAL:
                if ((this.lifetime & 15) == 15) {
                    world.addImportantParticle((ParticleEffect)type, true, pos.x, pos.y, pos.z, fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
                }
                break;
        }
    }

    @Internal
    public void emit() {
        if (world.isClient) {
            emitClient();
        } else {
            final Vec3d vec = new Vec3d(fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
            // TODO fix these offset and speed variables. I have no idea what they look like
            final ParticleS2CPacket packet = new ParticleS2CPacket((ParticleEffect)type, true, pos.x, pos.y, pos.z, (float)vec.x, (float)vec.y, (float)vec.z, (float)vec.length(), 0);
            forPlayerInRange(player -> {
                if (leachers.contains(player.getId())) {
                    player.networkHandler.connection.send(packet);
                } else if (!peers.contains(player.getId())) {
                    if (ServerPlayNetworking.canSend(player, Packets.SEND_PARTICLE_EMITTER)) {
                        peers.add(player.getId());
                        Packets.SERVER.sendParticleEmiiter(player, this.toPacket());
                    } else {
                        this.leachers.add(player.getId());
                    }
                }
            });
        }
    }

    @Internal
    public PacketByteBuf toPacket() {
        PacketByteBuf pbb = Packets.newPbb();
        pbb.writeIdentifier(Registries.PARTICLE_TYPE.getId(type.getType()));
        type.write(pbb);

        pbb.writeDouble(this.pos.x);
        pbb.writeDouble(this.pos.y);
        pbb.writeDouble(this.pos.z);
        pbb.writeString(this.stringVelocityX);
        pbb.writeString(this.stringVelocityY);
        pbb.writeString(this.stringVelocityZ);
        pbb.writeInt(this.lifetime);
        pbb.writeLong(this.ID);
        return pbb;
    }

    @Internal
    @SuppressWarnings({"unchecked","deprecation"})
    public static ParticleEmitter fromPacket(PacketByteBuf buf, World world) {
        ParticleType<ParticleEffect> type = (ParticleType<ParticleEffect>) Registries.PARTICLE_TYPE.get(buf.readIdentifier());
        
        return new ParticleEmitter(type.getParametersFactory().read(type, buf), new Vec3d(buf.readDouble(), buf.readDouble(), buf.readDouble()), buf.readString(), buf.readString(), buf.readString(), world, buf.readInt(), buf.readLong());
    }

    @Internal
    public void tick() {
        this.emit();
        if (--lifetime == 0) {
            this.kill();
        }
    }
}
