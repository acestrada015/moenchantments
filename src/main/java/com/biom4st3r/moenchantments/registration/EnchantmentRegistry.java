package com.biom4st3r.moenchantments.registration;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.logic.AlphafireLogic;
import com.biom4st3r.moenchantments.logic.ChaosArrowLogic;
import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.logic.Familiarity;
import com.biom4st3r.moenchantments.logic.RetainedPotion;
import com.biom4st3r.moenchantments.logic.Slippery;
import com.biom4st3r.moenchantments.logic.VeinMinerLogic;
import com.biom4st3r.moenchantments.util.DoMagicThing;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment.AnvilAction;
import biom4st3r.libs.moenchant_lib.builder.EnchantBuilder;
import biom4st3r.libs.moenchant_lib.events.BlockBreakEvent.BreakStage;
import biom4st3r.libs.moenchant_lib.events.LivingEntityDamageEvent;
import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity.PickupPermission;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;

/**
 * EnchantmentRegistry
 * Centeral store for Enchantment instances and registry
 */
public class EnchantmentRegistry implements EnchantmentPresets {
    public static final String MODID = ModInit.MODID;

    // 1 5 - 35
    // 2 10 - 40
    // 3 15 - 45
    public static final ExtendedEnchantment TREEFELLER = EnchantBuilder.newBuilder(Rarity.UNCOMMON, EquipmentSlot.MAINHAND)
        .maxlevel(MoEnchantsConfig.config.TreeFellerMaxBreakByLvl.length)
        .isAcceptible(isAxe)
        .minpower(level -> level*5)
        .enabled(true)
        .buildAndRegister("treefeller");
    // 1 5 - 35
    // 2 10 - 40
    // 3 15 - 45
    public static final ExtendedEnchantment VEINMINER = EnchantBuilder.newBuilder(Rarity.UNCOMMON, EquipmentSlot.MAINHAND)
        .maxlevel(MoEnchantsConfig.config.VeinMinerMaxBreakByLvl.length)
        .isAcceptible(isPickaxe)
        .minpower(level -> level*5)
        .maxpower(level -> level*10)
        // .preBlockBreak((stack,player,level) -> {
        //     VeinMinerLogic.tryVeinMining(player.getWorld(), blockPos, blockState, pe);
        //     return false;
        // })
        .onBlockBreak(BreakStage.POST_BREAK, (world,pos,player)-> {
            VeinMinerLogic.tryVeinMining(world, pos, world.getBlockState(pos), player);
            return true;
        })
        .enabled(true)
        .buildAndRegister("veinminer");
    // 1 30 - 50
    public static final ExtendedEnchantment AUTOSMELT = EnchantBuilder.newBuilder(Rarity.VERY_RARE, EquipmentSlot.MAINHAND)
        .addExclusive(Enchantments.SILK_TOUCH)
        .isAcceptible(isMiningTool)
        .addExclusive(Enchantments.SILK_TOUCH)
        .minpower(level -> 30)
        .maxpower(level -> 50)
        .onBlockBreak(BreakStage.POST_BREAK, (world,pos,player)-> {
            if(EnchantmentRegistry.AUTOSMELT.getLevel(player) > 0) {
                int exp = AlphafireLogic.removeExp(player.getMainHandStack());
                ModInit.logger.debug("retrieved XP %s", exp);
                for(;exp > 0; exp--) {
                    AlphafireLogic.dropExperience(1, pos, world);
                }
            }
            return true;
        })
        .enabled(true)
        .buildAndRegister("autosmelt");

    // 1 5 - 35
    public static final ExtendedEnchantment TAMEDPROTECTION = EnchantBuilder.newBuilder(Rarity.COMMON, EquipmentSlot.MAINHAND)
        .isAcceptible(isSword.or(isAxe).or(isBow).or(isCrossbow))
        .treasure(true)
        .minpower(level -> 5)
        .modifyDamageOutput((lvl, target, damage, source) -> {
            if (target instanceof TameableEntity defender && source.getAttacker() instanceof PlayerEntity player && defender.getOwnerUuid() != Familiarity.uuidZero) {
                if (MoEnchantsConfig.config.TameProtectsOnlyYourAnimals) {
                    if (defender.isOwner(player)) {
                        return LivingEntityDamageEvent.FAIL;
                    }
                } else {
                    return LivingEntityDamageEvent.FAIL;
                }
            }
            return LivingEntityDamageEvent.PASS;
        })
        .enabled(true)
        .buildAndRegister("tamedprotection");

    // 1 1
    // 2 20 - 50
    // 3 30 - 60
    public static final ExtendedEnchantment ENDERPROTECTION = EnchantBuilder.newBuilder(Rarity.VERY_RARE, ARMOR_SLOTS)
        .maxlevel(3)
        .minpower(level -> (level)*10)
        .maxpower(level ->  (level)*11)
        .isAcceptibleInAnvil((left,right,output,consumer) -> {
            if (left.getItem() == Items.ENCHANTED_BOOK) {
                return AnvilAction.PASS;
            } else if (right.getItem() == Items.ENCHANTED_BOOK) {
                return AnvilAction.PASS;
            } else if (EnchantmentRegistry.ENDERPROTECTION.getLevel(left) == EnchantmentRegistry.ENDERPROTECTION.getLevel(right)) {
                return AnvilAction.PASS;
            }
            return AnvilAction.REMOVE;
        })
        .isAcceptible(isArmor)
        .setApplicationLogic((enchant, power, stack, treasureAllowed, list) ->  {
            if (treasureAllowed != enchant.isTreasure()) return;
            if (enchant.isAcceptableItem(stack) || (stack.getItem() == Items.BOOK && ExtendedEnchantment.cast(enchant).isAcceptibleOnBook())) {
                for (int lvl = enchant.getMaxLevel(); lvl > enchant.getMinLevel() - 1; --lvl) {
                    if (power >= enchant.getMinPower(lvl) && power <= enchant.getMaxPower(lvl)) {
                        list.add(new EnchantmentLevelEntry(enchant, lvl == 3 ? 1 : lvl == 1 ? 3 : 2));
                        break;
                    }
                }
            }
        })
        .modifyIncomingDamage((lvl, target, damage, source) -> {
            if (lvl > 0 && EnderProtectionLogic.doLogic(source, lvl, target)) {
                return LivingEntityDamageEvent.FAIL;
            }
            return LivingEntityDamageEvent.PASS;
        })
        .treasure(true)
        .curse(true)
        .enabled(true)
        .build("curseofender");

    static {
        EnchantBuilder.modEnchantment(ENDERPROTECTION, builder->builder.addExclusive(ENDERPROTECTION));
        Registry.register(Registries.ENCHANTMENT, new Identifier(ModInit.MODID,ENDERPROTECTION.regName()), ENDERPROTECTION.asEnchantment());
    }

    //Gitlab @Mr Cloud
    // 1 21 - 51
    public static final ExtendedEnchantment SOULBOUND = EnchantBuilder.newBuilder(Rarity.UNCOMMON, ALL_SLOTS)
        .enabled(MoEnchantsConfig.config.EnableSoulbound)
        .maxlevel(MoEnchantsConfig.config.UseStandardSoulboundMechanics ? 1 : 10)
        .minpower(level -> 13 * level)
        .maxpower(level -> (int)(14.4 * level))
        .isAcceptible(YES)
        .treasure(true)
        .buildAndRegister("soulbound");

    public static final ExtendedEnchantment POTIONRETENTION = EnchantBuilder.newBuilder(Rarity.RARE, HAND_SLOTS)
        .minpower(level -> 7 * level)
        .maxlevel(10)
        .isAcceptible(isSword.or(isAxe))
        .modifyDamageOutput((lvl, target, damage, source) -> {
            RetainedPotion.borrowRetainedPotion(((LivingEntity)source.getAttacker()).getMainHandStack(), rt -> {
                rt.useCharge(target);
            });
            return LivingEntityDamageEvent.PASS;
        })
        .enabled(true)
        .buildAndRegister("potionretension");
    
    // someone responding to Jeb_ on reddit.
    // 1 10 - 40
    // 2 20 - 50
    public static final ExtendedEnchantment BOW_ACCURACY = EnchantBuilder.newBuilder(Rarity.RARE, HAND_SLOTS)
        .maxlevel(2)
        .applyEnchantmentToArrows()
        .minpower(level -> level*10)
        .enabled(!ModInit.extraBowsFound)
        .isAcceptible(isBow.or(isCrossbow))
        .buildAndRegister("bowaccuracy");

    public static final ExtendedEnchantment BOW_ACCURACY_CURSE = EnchantBuilder.newBuilder(Rarity.RARE, HAND_SLOTS)
        .maxlevel(1)
        .curse(true)
        .applyEnchantmentToArrows()
        .minpower(l -> 20)
        .maxpower(l -> 60)
        .addExclusive(BOW_ACCURACY)
        .isAcceptible(isBow.or(isCrossbow))
        .enabled(!ModInit.extraBowsFound)
        .buildAndRegister("bowinaccuracy");

    // Gitlab @cryum
    //1 30 - 60
    public static final ExtendedEnchantment ARROW_CHAOS = EnchantBuilder.newBuilder(Rarity.VERY_RARE, HAND_SLOTS)
        .curse(true)
        .isAcceptible(isBow.or(isCrossbow))
        .onArrowCreated((arrow, weapon, arrowStack, shooter) -> {
            if (ChaosArrowLogic.makePotionArrow(shooter, arrow, shooter.getRandom())) {
                arrow.pickupType = PickupPermission.CREATIVE_ONLY;
            }
        })
        .minpower(level -> 30)
        .enabled(!ModInit.extraBowsFound)
        .buildAndRegister("arrow_chaos");

    // cryum from issue #13
    // 1 16 - 46
    // 2 32 - 62
    public static final ExtendedEnchantment GRAPNEL = EnchantBuilder.newBuilder(Rarity.RARE, HAND_SLOTS)
        .minpower(level -> 20)
        .maxlevel(1)
        .treasure(true)
        .enabled(true)
        .applyEnchantmentToArrows()
        .modifyDamageOutput((lvl, target, damage, source) -> {
            return TypedActionResult.consume(0.5F);
        })
        .isAcceptible(isBow.or(isCrossbow))
        .addExclusive(Enchantments.MULTISHOT)
        .addExclusive(ARROW_CHAOS)
        .buildAndRegister("grapnel");

    // RM Issue #33
    // When a block is broke it goes straight into your inventory.
    public static final ExtendedEnchantment BLACK_HOLE = EnchantBuilder.newBuilder(Rarity.VERY_RARE, ALL_SLOTS)
        .maxlevel(1)
        .isAcceptible(isMiningTool)
        .minpower(lvl -> 30)
        .maxpower(lvl -> 37)
        .treasure(true)
        .enabled(true)
        .buildAndRegister("blackhole");

    // KORR Issue #48
    // When use or hit and the item you have a chance to drop it
    public static final ExtendedEnchantment SLIPPERY = EnchantBuilder.newBuilder(Rarity.VERY_RARE, HAND_SLOTS)
        .maxlevel(1)
        .isAcceptible(ItemStack::isDamageable)
        .treasure(true)
        .curse(true)
        .minpower(lvl -> 1)
        .maxpower(lvl -> 40)
        .modifyDamageOutput((lvl, target, damage, source) -> {
            if (source.getAttacker() instanceof LivingEntity attacker) {
                if (EnchantmentRegistry.SLIPPERY.hasEnchantment(attacker.getMainHandStack()) && Slippery.shouldDropOnAttack(attacker.getRandom())) {
                    Slippery.doDrop(attacker);
                    return TypedActionResult.consume(damage * MoEnchantsConfig.config.slippery_multiplier_when_item_dropped_during_attack);
                }
            }
            if (target instanceof LivingEntity && ((LivingEntity)target).isBlocking()) {
                LivingEntity defender = (LivingEntity) target;
                if (EnchantmentRegistry.SLIPPERY.hasEnchantment(defender.getActiveItem()) && Slippery.shouldDropOnBlockWithShield(defender.getRandom())) {
                    Slippery.doDrop(defender);
                }
            }
            return LivingEntityDamageEvent.PASS;
        })
        .onBlockBreak(BreakStage.PRE_BREAK, (world,pos,player)-> {
            if(EnchantmentRegistry.SLIPPERY.hasEnchantment(player.getMainHandStack()) && Slippery.shouldDropOnMine(world.random)) {
                Slippery.doDrop(player);
                return false;
            }
            return true;
        })
        .enabled(true)
        .buildAndRegister("slippery");

    // When using ender pearls instead of throwing the pearl you go to where you're looking
    public static final ExtendedEnchantment VOID_STEP = EnchantBuilder.newBuilder(Rarity.VERY_RARE, EquipmentSlot.FEET)
        .maxlevel(1)
        .isAcceptible(isShoe)
        .treasure(true)
        .enabled(true) // FabricLoader.getInstance().isDevelopmentEnvironment()
        .buildAndRegister("voidstep");

    // Eating things that would hurt you now help. Posion Potatos, Spider eyes
    public static final ExtendedEnchantment FILTER_FEEDER = EnchantBuilder.newBuilder(Rarity.VERY_RARE, EquipmentSlot.HEAD)
        .maxlevel(1)
        .isAcceptible(isHat)
        .treasure(true)
        .enabled(true)
        .buildAndRegister("filterfeeder");

    // hehe
    public static ExtendedEnchantment enchantment = EnchantBuilder.newBuilder(Rarity.VERY_RARE, EquipmentSlot.MAINHAND)
        .addExclusive(Enchantments.UNBREAKING, Enchantments.MENDING)
        .treasure(true)
        .fromLibrarian(false)
        .isAcceptible(stack -> stack.isOf(DoMagicThing.getTime()))
        .fromEnchantmentTableAndLoot(false)
        .enabled(true)
        .setName(i -> Text.of("Ritual").copy().formatted(Formatting.OBFUSCATED, Formatting.DARK_PURPLE))
        .buildAndRegister("ritual");
    
    public static final ExtendedEnchantment LIFE_LIKE = EnchantBuilder.newBuilder(Rarity.VERY_RARE, EquipmentSlot.MAINHAND)
        .treasure(true)
        .fromLibrarian(true)
        .minpower(level -> 30)
        .maxpower(level -> 50)
        .maxlevel(2)
        .fromEnchantmentTableAndLoot(false)
        .isAcceptible(LivingItemEntity.IS_ACCEPTIBLE)
        .enabled(true)
        .setName(level -> Text.translatable("enchantment.moenchantments.lifelike." + level).formatted(Formatting.GRAY))
        .buildAndRegister("lifelike")
    ;
    
    // static {
    //     try {
    //         // GodMode.GOD.unreflect(LibInit.CLAZZ.get().getMethod("overrideName", Text.class)).invoke(enchantment, );
    //     } catch(Throwable t) {
    //         t.printStackTrace();
    //     }
    // }


    public static void classLoad() {
    }
}