package com.biom4st3r.moenchantments;

import net.fabricmc.loader.api.FabricLoader;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

import biom4st3r.libs.biow0rks.BioLogger;
import biom4st3r.libs.moenchant_lib.LibInit;
import biom4st3r.libs.moenchant_lib.config_test.EnchantmentOverride;
import com.google.common.collect.Lists;

/**
 * Plugin
 */
public class Plugin implements IMixinConfigPlugin {

    private static final BioLogger logger = new BioLogger("MoEnchantmentMxnPlugin");
    private static final boolean adventure_plateform = FabricLoader.getInstance().isModLoaded("adventure-platform-fabric");

    @Override
    public String getRefMapperConfig() {
        return null;
    }
    
    static boolean filterFeederEnabled;
    static {
        if (LibInit.getOveride("filterfeeder").getOrNull() instanceof EnchantmentOverride override) {
            if (override.enabled != null) {
                filterFeederEnabled = override.enabled.booleanValue();
            } else {
                filterFeederEnabled = true;
            }
        } else {
            filterFeederEnabled = true;
        }
    }

    public static Predicate<String> DISABLER = s -> {
        String prefix = "com.biom4st3r.moenchantments.mixin.";

        if (s.contains(prefix+"soulbound/")) return MoEnchantsConfig.config.EnableSoulbound;
        if (s.contains(prefix + "filterfeeder/")) return filterFeederEnabled;
        if (s.equals(prefix + "bowaccuracy/ProjectileEntityMxn")) return !FabricLoader.getInstance().isModLoaded("extrabows");
        if (s.equals(prefix + "FillFix")) return FabricLoader.getInstance().isDevelopmentEnvironment();
        if (s.equals(prefix + "SkipEulaMxn")) return FabricLoader.getInstance().isDevelopmentEnvironment();
        if (s.equals(prefix + "SpeedDisplay")) return false;
        if (s.equals(prefix + "SlaughterWatchDogMxn")) return FabricLoader.getInstance().isDevelopmentEnvironment();
        if (s.equals(prefix + "FuckCommandManagerMxn")) return FabricLoader.getInstance().isDevelopmentEnvironment();
        if (s.equals(prefix + "moenchant_lib/EnchantmentHelperInProd")) return !FabricLoader.getInstance().isDevelopmentEnvironment();
        if (s.equals(prefix + "moenchant_lib/EnchantmentHelperInDev")) return FabricLoader.getInstance().isDevelopmentEnvironment();
        return true;
    };

    public static <T> T make(T obj, Consumer<T> consumer) {
        consumer.accept(obj);
        return obj;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        boolean b = DISABLER.test(mixinClassName);
        logger.debug("Applying %s: %s", mixinClassName, b);
        return b;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        List<String> EXTRA_MIXINS = Lists.newArrayList();

        // partical emitter lib
        EXTRA_MIXINS.add("particleemitterlib/WorldParticleEmitterMnx");
        // Compat
        if (adventure_plateform) EXTRA_MIXINS.add("compat/adventure_plateform/ServerPlayNetworkHandlerMxn");
        return EXTRA_MIXINS;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
        if (mixinClassName.equals("com.biom4st3r.moenchantments.mixin.compat/adventure_plateform/ServerPlayNetworkHandlerMxn")) {
            MethodNode mn = targetClass.methods.stream().filter(m -> m.name.contains("adventure$initTracking")).findFirst().get();
            mn.access = Opcodes.ACC_PROTECTED;
        }
    }

    static private boolean hasFlag(int access, int flag) {
        return (access & flag) != 0;
    }

    static Predicate<Integer> isFinal = i  ->  hasFlag(i, Opcodes.ACC_FINAL);
    static Predicate<Integer> isPublic = i  ->  hasFlag(i, Opcodes.ACC_PUBLIC);
    static Predicate<Integer> isPrivate = i  ->  hasFlag(i, Opcodes.ACC_PRIVATE);
    static Predicate<Integer> isProtected = i  ->  hasFlag(i, Opcodes.ACC_PROTECTED);
    static Predicate<Integer> isPackagePrivate = i  ->  !hasFlag(i, Opcodes.ACC_PUBLIC | Opcodes.ACC_PROTECTED | Opcodes.ACC_PRIVATE);
    static Predicate<Integer> isInterface = i  ->  hasFlag(i, Opcodes.ACC_INTERFACE);
    static Predicate<Integer> isEnum = i  ->  hasFlag(i, Opcodes.ACC_ENUM);

    // public void executeOrder66() {
    //     ScanResult result = new ClassGraph().enableAllInfo().scan();
    //     List<String> clazzes = Lists.newArrayList();
    //     result.getAllClasses().filter(clazz  ->  clazz.getName().startsWith("net.minecraft")).forEach(clazzInfo  ->  {
    //         clazzes.add(clazzInfo.getName().replace(".", "/"));
    //     });
    //     List<String> widener = Lists.newArrayList("accessWidener\tv1\tnamed");
    //     clazzes
    //         // Stream of Class name strings
    //         .stream()//.filter(c -> c.startsWith("net/minecraft/loot"))
    //         .map(clazz  ->  {
    //             ClassReader reader = null;
    //             try {
    //                 reader = new ClassReader(clazz);
    //             } catch (IOException e) {
    //                 e.printStackTrace();
    //             }
    //             ClassNode cnode = new ClassNode(Opcodes.ASM8);
    //             reader.accept(cnode, ClassReader.SKIP_CODE|ClassReader.SKIP_DEBUG|ClassReader.SKIP_FRAMES);
    //             return cnode;
    //         })
    //         .filter(cn -> isInterface.negate().test(cn.access) )
    //         .forEach(cn ->
    //         {
    //             if (isPublic.negate().test(cn.access)) {
    //                 if (isEnum.test(cn.access)) widener.add("# Enum");
    //                 // if (isInterface.test(cn.access)) widener.add("# Interface");
    //                 if (isPackagePrivate.test(cn.access)) widener.add("# Package Private");
    //                 widener.add(String.format("%s\t%s\t%s", "accessible","class", cn.name));
    //             }
    //             if (isFinal.test(cn.access)) {
    //                 widener.add(String.format("%s\t%s\t%s", "extendable","class", cn.name));
    //             }

    //             if (isEnum.test(cn.access)) return;
                
    //             cn.methods
    //                 .stream()//.filter(mn -> false)
    //                 .forEach(mn ->
    //                 {
    //                     if (isPublic.negate().test(mn.access)) {
    //                         widener.add(String.format("%s\t%s\t%s\t%s\t%s", "accessible","method", cn.name, mn.name, mn.desc));
    //                     }
    //                     if (isFinal.test(mn.access)) {
    //                         widener.add(String.format("%s\t%s\t%s\t%s\t%s", "extendable","method", cn.name, mn.name, mn.desc));
    //                     }
    //                 });
    //             cn.fields
    //                 .stream()//.filter(mn -> false)
    //                 .forEach(fn ->
    //                 {
    //                     if (isPublic.negate().test(fn.access)) {
    //                         widener.add(String.format("%s\t%s\t%s\t%s\t%s", "accessible","field", cn.name, fn.name, fn.desc));
    //                     }
    //                     if (isFinal.test(fn.access)) {
    //                         widener.add(String.format("%s\t%s\t%s\t%s\t%s", "mutable","field", cn.name, fn.name, fn.desc));
    //                     }
    //                 });
    //             if (isPublic.negate().or(isFinal).test(cn.access)) widener.add("\n");
    //         });
    //     File file = new File(FabricLoader.getInstance().getConfigDir().toString(),"widenme.baby");
    //     try {
    //         System.out.println("writing widener");
    //         FileWriter writer = new FileWriter(file);
    //         writer.write(String.join("\n", widener));//.stream()/*.filter(s -> !s.startsWith("#"))*/.collect(Collectors.toList())));
    //         writer.close();
    //     } catch (IOException e) {
    //         e.printStackTrace();
    //     }
    // }

    @Override
    public void onLoad(String mixinPackage) {
        // ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        // OpcodeClassVisitor cv = OpcodeMethodVisitor.newCv(cw);
        // cv.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, "FFFF", null, "Ljava/lang/Object;", null);
        // OpcodeMethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC|Opcodes.ACC_STATIC|Opcodes.ACC_FINAL, "FFFF", "()V", null, null);
        // {
        //     mv.visitCode();

        //     mv.RETURN();
        //     mv.visitMaxs(1, 1);

        //     mv.visitEnd();
        // }
    }
}