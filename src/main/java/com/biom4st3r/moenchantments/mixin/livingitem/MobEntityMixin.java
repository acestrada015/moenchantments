package com.biom4st3r.moenchantments.mixin.livingitem;

import java.util.stream.Stream;

import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.biom4st3r.moenchantments.util.TagHelper;

import net.minecraft.entity.EntityData;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.registry.Registries;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.World;

@Mixin({MobEntity.class})
public abstract class MobEntityMixin extends LivingEntity {
    protected MobEntityMixin(EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(at = @At("RETURN"), method = "initialize")
    private void onInitlizeMaybeSpawnLIE(ServerWorldAccess world, LocalDifficulty difficulty, SpawnReason spawnReason, @Nullable EntityData entityData, @Nullable NbtCompound entityNbt, CallbackInfoReturnable<EntityData> ci) {
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return;
        if ((Object)this instanceof VillagerEntity && spawnReason == SpawnReason.STRUCTURE && world.getRandom().nextInt(LivingItemEntity.SPAWN_WITH_VILLAGER_CHANCE) == 0) {
            Item[] items = Stream.of(
                TagHelper.toStream(Registries.ITEM, TagHelper.item("c:hoes")),
                // LivingItemEntity.toItemStream(TagHelper.item("c:axes")),
                TagHelper.toStream(Registries.ITEM, TagHelper.item("c:pickaxes")))
                .flatMap(s -> s)
                .toArray(Item[]::new);
            ItemStack stack = new ItemStack(items[world.getRandom().nextInt(items.length)]);
            stack.addEnchantment(EnchantmentRegistry.LIFE_LIKE.asEnchantment(), 1);
            LivingItemEntity entity = new LivingItemEntity(this.getWorld(), stack, (LivingEntity)(Object)this);
            entity.refreshPositionAndAngles(this.getBlockPos(), this.getYaw(), this.getPitch());
            world.spawnEntity(entity);
        }
    }
}
