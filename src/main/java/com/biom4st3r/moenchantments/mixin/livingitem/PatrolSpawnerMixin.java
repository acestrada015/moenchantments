package com.biom4st3r.moenchantments.mixin.livingitem;

import java.util.List;

import com.biom4st3r.moenchantments.entities.LivingItemEntity;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.google.common.collect.Lists;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.block.BlockState;
import net.minecraft.entity.mob.PatrolEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

import net.minecraft.util.math.random.Random;
import net.minecraft.world.spawner.PatrolSpawner;

@Mixin({PatrolSpawner.class})
public class PatrolSpawnerMixin {
    @Inject(
        at = @At(value = "RETURN", ordinal = 2), 
        method = "spawnPillager",
        locals = LocalCapture.CAPTURE_FAILHARD
    )
    private void alsoSpawnLivingItemEntity(
        ServerWorld world, BlockPos pos, Random random, 
        boolean captain, CallbackInfoReturnable<Boolean> ci,
        BlockState blockState, PatrolEntity patrolEntity
    ) {
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return;
        if (ci.getReturnValueZ() && captain && world.getRandom().nextInt(LivingItemEntity.SPAWN_WITH_PATROL_CHANCE) == 0) {
            List<Item> list = Lists.newArrayList(Items.IRON_AXE, Items.STONE_AXE, Items.GOLDEN_AXE, Items.WOODEN_AXE, Items.DIAMOND_AXE,
            Items.IRON_PICKAXE, Items.STONE_PICKAXE, Items.GOLDEN_PICKAXE, Items.WOODEN_PICKAXE, Items.DIAMOND_PICKAXE);
            Item i = list.get(world.random.nextInt(list.size()));
            ItemStack is = new ItemStack(i);
            is.addEnchantment(EnchantmentRegistry.LIFE_LIKE.asEnchantment(), 1);
            LivingItemEntity lie = new LivingItemEntity(world, is, patrolEntity);
            lie.updatePositionAndAngles(patrolEntity.getX(), patrolEntity.getY(), patrolEntity.getZ(), patrolEntity.getYaw(), patrolEntity.getPitch());
            world.spawnEntity(lie);
        }
    }
}
