package com.biom4st3r.moenchantments.mixin.filterfeeder;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.SimpleDefaultedRegistry;
import net.minecraft.registry.entry.RegistryEntry.Reference;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import com.biom4st3r.moenchantments.logic.FilterFeeder.DUMMYFOODITEM;


@Mixin({Item.class})
public class ItemMxn {
    // Don't look like i need this anymore
    // @Redirect(method = "<init>*", at = @At(value = "INVOKE", 
    //     target = "net/minecraft/util/registry/DefaultedRegistry.createEntry(Ljava/lang/Object;)Lnet/minecraft/util/registry/RegistryEntry$Reference;"))
    // private Reference<Item> donotCreateEntry(SimpleDefaultedRegistry<Item> registry, Object item) {
    //     if (((Object)item) instanceof DUMMYFOODITEM) {
    //         return (Reference<Item>) ItemStack.EMPTY.getRegistryEntry();
    //     }
    //     // Registry.ITEM.createEntry(value)
    //     return registry.createEntry((Item) item);
    // }
}
