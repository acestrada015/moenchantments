package com.biom4st3r.moenchantments.mixin.filterfeeder;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.biom4st3r.moenchantments.logic.FilterFeeder;
import com.biom4st3r.moenchantments.logic.FilterFeeder.DUMMYFOODITEM;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({LivingEntity.class})
public abstract class LivingEntityMxn {
    private DUMMYFOODITEM moenchatnements$borrowedItem;
    @ModifyVariable(
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/item/ItemStack.getItem()Lnet/minecraft/item/Item;",
            shift = Shift.BY,
            by = 2), // INVOKE, ASTORE
        method = "applyFoodEffects",
        index = 4)
    private Item moenchantment$replaceItemFromFilterFeeder(Item i) {
        if (FilterFeeder.hasValidItemAndServer(this)) {
            moenchatnements$borrowedItem = FilterFeeder.borrow(i.getFoodComponent());
            return moenchatnements$borrowedItem;
        }
        return i;
    }
    @Inject(
        at = @At("TAIL"), 
        method = "applyFoodEffects", 
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void moenchantments$releaseBorrowedItem(ItemStack stack, World world, LivingEntity targetEntity, CallbackInfo ci) {
        if (moenchatnements$borrowedItem != null) {
            moenchatnements$borrowedItem.release();
            moenchatnements$borrowedItem = null;
        }
    }
}
