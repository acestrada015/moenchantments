package com.biom4st3r.moenchantments.mixin.grapnel;

import com.biom4st3r.moenchantments.logic.GrapnelLogic;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.world.World;

import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;

@Mixin(PersistentProjectileEntity.class)
public abstract class PersistentProjectileEntityMxn implements EnchantableProjectileEntity {
    @Inject(
        at = @At("TAIL"), 
        method = "onEntityHit")
    private void grapnelOnEntityHit(EntityHitResult entityHitResult, CallbackInfo ci) {
        World world = ((PersistentProjectileEntity)(Object)this).world;
        Entity target = entityHitResult.getEntity();
        if (((EnchantableProjectileEntity)this).getEnchantmentLevel(EnchantmentRegistry.GRAPNEL) > 0) {
            for (int i = 0; i < 10 && world.isClient; i++) {
                target.getEntityWorld().addParticle(ParticleTypes.ENCHANT, true, target.getX(), target.getY(), target.getZ(), target.world.random.nextFloat()*2-1, target.world.random.nextFloat()*2-1, target.world.random.nextFloat()*2-1);
            }
            GrapnelLogic.applyVelocity(target, ((PersistentProjectileEntity)(Object)this).getOwner());
        }
    }
    @Inject(
        at = @At("TAIL"), 
        method = "onBlockHit")
    public void grapnelOnBlockHit(BlockHitResult blockHitResult, CallbackInfo ci) {
        Entity target = ((PersistentProjectileEntity)(Object)this).getOwner();
        World world = ((PersistentProjectileEntity)(Object)this).world;
        if (target != null && target instanceof PlayerEntity && EnchantmentRegistry.GRAPNEL.hasEnchantment(((PlayerEntity)target).getActiveItem())) {
            for (int i = 0; i < 10 && world.isClient; i++) {
                target.getEntityWorld().addParticle(ParticleTypes.ENCHANT, true, target.getX(), target.getY(), target.getZ(), target.world.random.nextFloat()*2-1, target.world.random.nextFloat()*2-1, target.world.random.nextFloat()*2-1);
            }
            GrapnelLogic.applyVelocity(target, ((PersistentProjectileEntity)(Object)this));
            this.setEnchantmentLevel(EnchantmentRegistry.GRAPNEL, -1);
        }
        else if (target != null && ((EnchantableProjectileEntity)this).getEnchantmentLevel(EnchantmentRegistry.GRAPNEL) > 0) {
            for (int i = 0; i < 10 && world.isClient; i++) {
                target.getEntityWorld().addParticle(ParticleTypes.ENCHANT, true, target.getX(), target.getY(), target.getZ(), target.world.random.nextFloat()*2-1, target.world.random.nextFloat()*2-1, target.world.random.nextFloat()*2-1);
            }
            GrapnelLogic.applyVelocity(target, ((PersistentProjectileEntity)(Object)this));
            this.setEnchantmentLevel(EnchantmentRegistry.GRAPNEL, -1);
            
        }
    }
}