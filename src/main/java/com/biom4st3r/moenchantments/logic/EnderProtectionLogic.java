package com.biom4st3r.moenchantments.logic;

import net.minecraft.command.argument.EntityAnchorArgumentType.EntityAnchor;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;

public final class EnderProtectionLogic 
{
    public static void events() {
    }

    private static boolean isIdealSpace(BlockPos origin, World world, BlockPos pos) {
        if (origin.getManhattanDistance(pos) > MoEnchantsConfig.config.curse_of_the_end_teleport_range * 1.5) return false;
        else if (origin.getManhattanDistance(pos) < 4) return false;
        BlockPos standing = pos.down();
        boolean fluid = !world.getBlockState(standing).getFluidState().isEmpty();
        boolean isSolid = world.getBlockState(standing).isSolidBlock(world, standing);
        if (!isSolid || fluid) return false; // notFluid
        for (BlockPos loc : new BlockPos[] {pos, pos.up()}) {
            if (!world.isAir(loc)) return false;
        }
        return true;
    }

    private static void teleportAround(BlockPos pos, World world, LivingEntity defender) {
        double x, y, z;
        BlockPos.Mutable  mut = new BlockPos.Mutable();
        int iterations = 0;
        do {
            x = pos.getX() + (world.getRandom().nextDouble() - 0.5D) * MoEnchantsConfig.config.curse_of_the_end_teleport_range;
            y = pos.getY() + ((world.getRandom().nextDouble() * 20) - 5);
            z = pos.getZ() + (world.getRandom().nextDouble() - 0.5D) * MoEnchantsConfig.config.curse_of_the_end_teleport_range;
            mut.set(x, y, z);
            if (iterations++ > 150) { // Infinite loop Guard
                ModInit.logger.error("Curse of the End: Failed to find teleport location.");
                return;
            }
        } while(!isIdealSpace(pos, world, mut.toImmutable())); //  && iterations < 100

        defender.setVelocity(0, 0, 0);
        defender.velocityDirty = true;
        defender.playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 1f, 1f);
        defender.world.playSound((PlayerEntity)null, defender.prevX, defender.prevY, defender.prevZ, SoundEvents.ENTITY_ENDERMAN_TELEPORT, defender.getSoundCategory(), 1.0F, 1.0F);
        defender.requestTeleport(x + 0.5, y, z + 0.5);
        world.sendEntityStatus(defender, (byte) 46);
    }

    public static boolean teleport(LivingEntity attacker, LivingEntity defender, boolean alwaysteleport) {
        if (alwaysteleport || MoEnchantsConfig.config.chanceForEnderCurseToTeleportv2 > defender.getRandom().nextDouble()) {
            teleportAround(attacker.getBlockPos(), attacker.world, defender);
            return true;
        }
        return false;
    }

    public static void lookAtAttacker(LivingEntity attacker, LivingEntity defender) {
        defender.lookAt(EntityAnchor.FEET, attacker.getPos()); // Why does feet look at face?
    }

    public static boolean preventDamage(LivingEntity defender) {
        if (MoEnchantsConfig.config.chanceForEnderCurseToPreventDamagev2 > defender.getRandom().nextDouble()) {
            return true;
        }
        return false;
    }

    public static boolean doLogic(DamageSource damageSource, int protectionLvl, LivingEntity defender) {
        boolean teleported = false;
        if (damageSource.isProjectile()) {
            switch(protectionLvl) {
                case 1:
                    //break;¯\_(ツ)_/¯
                case 2:
                    if (damageSource.getAttacker() != null && damageSource.getAttacker() instanceof LivingEntity attacker) {
                        teleported = EnderProtectionLogic.teleport(attacker, defender, true);
                        EnderProtectionLogic.lookAtAttacker(attacker, defender);
                    }
                    else {
                        teleported = teleport(defender, defender, true);
                    }
                    break;
                default:
                    EnderProtectionLogic.teleport(defender, defender, true);
                    break;
            }
            if (protectionLvl < 3 && teleported) {
                return true;
            }
        } else if (damageSource.getAttacker() != null && damageSource.getAttacker() instanceof LivingEntity attacker) {
            switch(protectionLvl) {
                case 1:
                    teleported = EnderProtectionLogic.teleport(attacker, defender, false);
                    EnderProtectionLogic.lookAtAttacker(attacker, defender);
                    return EnderProtectionLogic.preventDamage(defender);
                case 2:
                    teleported = EnderProtectionLogic.teleport(attacker, defender, false);
                    EnderProtectionLogic.lookAtAttacker(attacker, defender);
                    break;
                default:
                    teleported = EnderProtectionLogic.teleport(attacker, defender, false);
                    break;
            }
        }
        return false;
    }





}