package com.biom4st3r.moenchantments.loot;

import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

import com.google.common.collect.Lists;

import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.util.Util;
import net.minecraft.util.math.MathHelper;

public class EnchantmentTable {
    // Table<EnchantmentLevelEntry> t = getPossible(31, new ItemStack(Items.DIAMOND_HELMET), false);
    // Map<String,Integer> map = Maps.newHashMap();
    // Random random = new Random();
    // for (int x = 0; x < 1000; x++) {
    //     EnchantmentLevelEntry e = t.roll(random);
    //     String s = Registry.ENCHANTMENT.getId(e.enchantment).toString() + " " + e.level;
    //     if (map.containsKey(s)) {
    //         map.put(s, map.get(s) + 1);
    //     } else {
    //         map.put(s, 1);
    //     }
    // }
    // map.forEach((e,i) -> {
    //     System.out.println(e + " : " + i);
    // });
    
    public static final CtxKey<Boolean> TREASURE = new CtxKey<>("enchant:treasure");
    public static final CtxKey<Integer> POWER = new CtxKey<>("enchant:power");
    public static final CtxKey <ItemStack> STACK = new CtxKey<>("enchant:stack");

    public static void test(int power, ItemStack stack, boolean treasureAllowed) {
        Table<EnchantmentLevelEntry> table = getAllEnchantmentStates();
        Random random = new Random();
        TableCtx ctx = new TableCtx()
            .add(POWER, power)
            .add(TREASURE, treasureAllowed)
            .add(STACK, stack);
        table.roll(random, ctx);
    }

    public static Table<EnchantmentLevelEntry> getAllEnchantmentStates() {
        TableBuilder<EnchantmentLevelEntry> builder = new TableBuilder<>();
        Registries.ENCHANTMENT.forEach(enchantment -> {
            for(int i = 1; i <= enchantment.getMaxLevel(); i++) {
                final int currlvl = i;
                Predicate<TableCtx> lvl = ctx -> {
                    int power = ctx.get(POWER);
                    return power > enchantment.getMinPower(currlvl) && power < enchantment.getMaxPower(currlvl);
                };
                lvl = lvl.and(ctx -> enchantment.isAcceptableItem(ctx.get(STACK)));
                if (enchantment.isTreasure()) {
                    lvl = lvl.and(ctx -> ctx.get(TREASURE));
                }
                builder.add(Entry.of(new EnchantmentLevelEntry(enchantment, i), enchantment.getRarity().getWeight(), lvl));
            }
        });
        return builder.build();
    }

    static Table<EnchantmentLevelEntry> table;

    private static EnchantmentLevelEntry plusOne(EnchantmentLevelEntry entry) {
        if (entry.level == entry.enchantment.getMaxLevel()) return entry;
        return new EnchantmentLevelEntry(entry.enchantment, entry.level);
    }

    public static void removeConflicts(List<EnchantmentLevelEntry> entry) {
        EnchantmentLevelEntry last = Util.getLast(entry);
        entry.remove(entry.size()-1);
        // Iterator<EnchantmentLevelEntry> iter = entry.iterator();
        for (int i = 0; i < entry.size(); i++) {
            EnchantmentLevelEntry current = entry.get(i);
            if (current.enchantment == last.enchantment) {
                if (last.level > current.level) {
                    entry.set(i, last);
                } else if (last.level == current.level) {
                    entry.set(i, plusOne(last));
                }
                return;
            } else if (!current.enchantment.canCombine(last.enchantment)) {
                return;
            } else if (!last.enchantment.canCombine(current.enchantment)) {
                return;
            }
        }

        entry.add(last);
    }

    /**
     * Mimics vanilla with enchnatment loot table
     * @param random
     * @param stack
     * @param level
     * @param treasureAllowed
     * @return
     */
    public static List<EnchantmentLevelEntry> EnchantmentHelper$generateEnchantment(Random random, ItemStack stack, int level, boolean treasureAllowed) {
        List<EnchantmentLevelEntry> list = Lists.newArrayList();
        int i = stack.getItem().getEnchantability();
        level += 1 + random.nextInt(i / 4 + 1) + random.nextInt(i / 4 + 1);
        float f = (random.nextFloat() + random.nextFloat() - 1.0F) * 0.15F;
        TableCtx ctx = new TableCtx()
            .add(POWER, MathHelper.clamp(Math.round(level + level * f), 1, Integer.MAX_VALUE))
            .add(TREASURE, treasureAllowed)
            .add(STACK, stack);
        if (table == null) {
            table = getAllEnchantmentStates();
        }
        list.add(table.roll(random, ctx));
        while (random.nextInt(50) <= level) {
            // EnchantmentHelper.removeConflicts(list, Util.getLast(list));
            list.add(table.roll(random, ctx));
            removeConflicts(list);
            level /= 2;
        }
        return list;
    }
}
