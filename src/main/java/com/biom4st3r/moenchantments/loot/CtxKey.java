package com.biom4st3r.moenchantments.loot;

public final record CtxKey<T>(String s) {
    @Override
    public String toString() {
        return s;
    }
}
