package com.biom4st3r.moenchantments.client;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.entities.client.LivingItemRenderer;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;

public class ClientModInit implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        ClientEventHandlers.init();
        EntityRendererRegistry.register(ModInit.LIVING_ITEM_TYPE, LivingItemRenderer::new);
    }
}
