## Slippery
* Curse
* Item has a chance to do dropped when used
## Treefeller
* Chops down whole trees(progressively larger per level)
    - Compatible with Alpha Fire
## Vein Miner
* Mines all nearby similar
    - Compatible with Alpha Fire
## Alpha Fire
* Automatically smelts and drops exp for blocks
    - Compatible with fortune
## Familiarity
* Prevents this weapon from damaging your pets or (configurably) all pets
## Curse of the End
* Gives you similar abilies to enderman
* becomes progressively more useless the heighter the level. more cursed == more bad
## Soulbound
*  has 2 modes: Default and Traditional:
    - Default: Has a configurable max level and everytime it would be dropped on death instead it losses a lvl
    - Traditional: max level is 1 and never decreases. Just protects item from being dropped on death
## Imbued
* Cofigurable max level
* When a potion effect is applied while holding the item tokens of that effect are stored in the item. This effect can be applied to the holder via right click and the effect is applied to attacked entities
## Marksman
* level 1 increases accuracy
* level 2 is perfect accuracy
## Curse of the Trooper
* Accuracy is drastically decreased
## Chaos
* Addes a random effect to arrows shot by this item
## Grapnel
* Pulls hit entities to the player 
* or
* Pulls player to arrow. Works with elytra
## Blackhole
* Attempted to place mined blocks into inventory
## Filter Feeder
* Converts bad effects of consumed food to positive effects
## Slippery
* Adds a chance to drop the item when used.
## Void Walker
* When using ender pearls while wearing boots with this enchantment the boots consume the pearl and teleport directly where you're looking.